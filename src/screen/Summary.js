import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {transcation} from '../data/data';

const Summary = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 56,
          elevation: 5,
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon
            name="arrowleft"
            size={20}
            color="#000000"
            style={{marginLeft: 25}}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontWeight: '700',
            color: 'black',
            fontSize: 18,
            marginLeft: 14,
          }}>
          {' '}
          Summary
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          height: 153,
          backgroundColor: 'white',
          marginTop: 6,
        }}>
        <Text
          style={{
            marginLeft: 26,
            fontWeight: '500',
            fontSize: 14,
            color: '#979797',
            marginTop: 20,
          }}>
          Data Customer
        </Text>
        <Text
          style={{
            marginLeft: 26,
            fontWeight: '400',
            fontSize: 14,
            color: 'black',
            marginTop: 6,
          }}>
          Agil 24443434343
        </Text>
        <Text
          style={{
            marginLeft: 26,
            fontWeight: '400',
            fontSize: 14,
            color: 'black',
            marginTop: 6,
          }}>
          Jalan Jalan
        </Text>
        <Text
          style={{
            marginLeft: 26,
            fontWeight: '400',
            fontSize: 14,
            color: 'black',
            marginTop: 6,
          }}>
          GantengDoang@dipanggang.com
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          height: 103,
          backgroundColor: 'white',
          marginTop: 6,
        }}>
        <Text
          style={{
            marginLeft: 26,
            fontWeight: '500',
            fontSize: 14,
            color: '#979797',
            marginTop: 20,
          }}>
          Alamat Outlet Tujuan
        </Text>

        <Text
          style={{
            marginLeft: 26,
            fontWeight: '400',
            fontSize: 14,
            color: 'black',
            marginTop: 6,
          }}>
          Jack Repair - Seturan (0274) 1234
        </Text>
        <Text
          style={{
            marginLeft: 26,
            fontWeight: '400',
            fontSize: 14,
            color: 'black',
            marginTop: 6,
          }}>
          Jalan Affandi
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          height: 172,
          backgroundColor: 'white',
          marginTop: 4,
        }}>
        <Text
          style={{
            marginLeft: 26,
            fontWeight: '500',
            fontSize: 14,
            color: '#979797',
            marginTop: 20,
          }}>
          Barang
        </Text>
        <View style={{flexDirection: 'row', marginTop: 16}}>
          <Image
            style={{
              width: 84,
              height: 84,
              borderRadius: 10,
              marginLeft: 27,
            }}
            source={{
              uri: 'https://images.tokopedia.net/img/cache/500-square/VqbcmM/2022/5/18/958fecd5-66da-44e6-9e3c-21a8d2bcd16f.jpg',
            }}
          />
          <View style={{marginLeft: 20}}>
            <Text style={{fontWeight: '500', color: '#000000', fontSize: 12}}>
              New Balance
            </Text>
            <Text style={{fontWeight: '400', fontSize: 12, marginTop: 11}}>
              Cuci Sepatu
            </Text>
            <Text style={{fontWeight: '400', fontSize: 12, marginTop: 11}}>
              Note : -
            </Text>
          </View>
        </View>
      </View>
      <View
        style={{
          backgroundColor: '#BB2427',
          height: 55,
          width: 335,
          bottom: 20,
          position: 'absolute',
          alignSelf: 'center',
          borderRadius: 10,
        }}>
        <TouchableOpacity
          onPress={() => navigation.navigate('CheckOut')}
          style={{
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            height: 55,
            width: 335,
          }}>
          <Text style={{fontWeight: '700', fontSize: 16, color: 'white'}}>
            Reservasi Sekarang
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default Summary;
