import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import moment from 'moment/moment';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
  TextInput,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import {convertCurrency} from '../utils/Helpers';

const TambahToko = ({navigation, route}) => {
  const [title, setTitle] = useState('');
  const [address, setAddress] = useState('');
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [rating, setRating] = useState('');
  const [selectedTimeField, setSelectedTimeField] = useState('');
  const [openingTime, setOpeningTime] = useState('');
  const [closingTime, setClosingTime] = useState('');
  const [imgURL, setImgURL] = useState('');
  const [isOpen, setIsOpen] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const dispatch = useDispatch();
  const {storeData} = useSelector(state => state.home);
  const options = [
    {label: 'Buka', value: true},
    {label: 'Tutup', value: false},
  ];
  const checkData = () => {
    if (route.params) {
      const data = route.params;
      setTitle(data.title);
      setAddress(data.address);
      setMinPrice(data.minPrice);
      setMaxPrice(data.maxPrice);
      setRating(data.rating);
      setOpeningTime(data.openingTime);
      setClosingTime(data.closingTime);
      setImgURL(data.imgURL);
      setIsOpen(data.isOpen);
    }
  };
  const deleteData = async () => {
    dispatch({type: 'DELETE_DATA', id: route.params.idStore});
    navigation.navigate('Home');
  };
  React.useEffect(() => {
    checkData();
  }, []);
  const updateData = () => {
    const data = {
      idStore: route.params.idStore,
      rating: rating,
      title: title,
      openingTime: openingTime,
      closingTime: closingTime,
      address: address,
      isOpen: isOpen,
      minPrice: minPrice,
      maxPrice: maxPrice,
      imageUrl: imgURL
        ? imgURL
        : 'https://cdn1-production-images-kly.akamaized.net/JEMj4EuE3Asq25jLBoDQJgqbU3o=/1200x900/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/4030176/original/070314900_1653232307-1dd5bc38-5a69-4865-b27e-ec98d1a4666e.jpg',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.',
    };
    dispatch({type: 'UPDATE_DATA', data});
    console.log('nyobaa', data);
    navigation.navigate('Home');
  };

  const RadioButton = ({options, selectedOption, onSelect}) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
        }}>
        {options.map(option => (
          <TouchableOpacity
            key={option.value}
            style={{flexDirection: 'row', alignItems: 'center'}}
            onPress={() => onSelect(option)}>
            <View
              style={{
                width: 20,
                height: 20,
                borderRadius: 10,
                borderWidth: 2,
                marginTop: 3,
                borderColor:
                  option.value === selectedOption ? '#BB2427' : 'gray',
                marginRight: 10,
                backgroundColor:
                  option.value === selectedOption ? '#BB2427' : 'gray',
              }}></View>
            <Text>{option.label}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const showTimePicker = field => {
    setTimePickerVisibility(true);
    setSelectedTimeField(field);
  };

  const hideTimePicker = () => {
    setTimePickerVisibility(false);
  };

  const handleConfirm = time => {
    const formattedTime = time.toLocaleTimeString([], {
      hour: '2-digit',
      minute: '2-digit',
      hour12: false,
    });
    const timeWithoutSeconds = formattedTime.slice(0, -3);
    console.log(timeWithoutSeconds);
    if (selectedTimeField === 'openingTime') {
      setOpeningTime(formattedTime);
    } else if (selectedTimeField === 'closingTime') {
      setClosingTime(formattedTime);
    }

    hideTimePicker();
  };
  const tambahData = () => {
    var dataStore = [...storeData];
    const data = {
      idStore: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      rating: rating ? rating : 1,
      title: title ? title : 'Jack Fixer',
      openingTime: openingTime,
      closingTime: closingTime,
      address: address ? address : 'Alamat Tidak diketahui',
      latitude: -7.796876,
      longitude: 110.365745,
      isOpen: isOpen,
      favourite: true,
      minPrice: minPrice ? minPrice : 0,
      maxPrice: maxPrice ? maxPrice : 0,
      imageUrl: imgURL
        ? imgURL
        : 'https://cdn1-production-images-kly.akamaized.net/JEMj4EuE3Asq25jLBoDQJgqbU3o=/1200x900/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/4030176/original/070314900_1653232307-1dd5bc38-5a69-4865-b27e-ec98d1a4666e.jpg',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa gravida mattis arcu interdum lectus egestas scelerisque. Blandit porttitor diam viverra amet nulla sodales aliquet est. Donec enim turpis rhoncus quis integer. Ullamcorper morbi donec tristique condimentum ornare imperdiet facilisi pretium molestie.',
    };
    dataStore.push(data);
    dispatch({type: 'ADD_DATA', data: dataStore});
    navigation.goBack();
  };

  return (
    <View style={{flex: 1, alignItems: 'center', backgroundColor: 'white'}}>
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 56,
          elevation: 5,
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
          paddingHorizontal: 10,
        }}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon name="arrowleft" size={20} color="#000000" />
        </TouchableOpacity>
        <Text
          style={{
            fontWeight: '700',
            color: 'black',
            fontSize: 18,
            marginLeft: 10,
          }}>
          {route.params ? 'Edit Toko' : 'Tambah Toko'}
        </Text>
      </View>
      <ScrollView
        style={{width: '100%', height: '100%', paddingHorizontal: 20}}>
        <Text
          style={{
            color: '#BB2427',
            marginBottom: 12,
            fontWeight: '600',
            fontSize: 12,
            marginTop: 20,
          }}>
          Nama Toko
        </Text>
        <View
          style={{
            backgroundColor: '#F6F8FF',
            height: 45,
            width: '100%',

            marginBottom: 27,
            borderWidth: 0.4,
            borderColor: '#BB2427',
            borderRadius: 5,
          }}>
          <TextInput
            defaultValue={title}
            placeholder="Masukkan Nama Toko"
            onChangeText={text => setTitle(text)}
          />
        </View>
        <Text
          style={{
            color: '#BB2427',
            marginBottom: 12,
            fontWeight: '600',
            fontSize: 12,
          }}>
          Alamat
        </Text>
        <View
          style={{
            backgroundColor: '#F6F8FF',
            height: 45,
            width: '100%',

            marginBottom: 20,
            borderWidth: 0.4,
            borderColor: '#BB2427',
            borderRadius: 5,
          }}>
          <TextInput
            defaultValue={address}
            placeholder="Masukkan Alamat"
            onChangeText={text => setAddress(text)}
          />
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View>
            <Text
              style={{
                color: '#BB2427',
                marginBottom: 6,
                fontWeight: '600',
                fontSize: 12,
              }}>
              Harga Minimal
            </Text>
            <Text
              style={{
                color: '#BB2427',
                marginBottom: 6,
                fontWeight: '600',
                fontSize: 12,
              }}>
              {convertCurrency(minPrice, 'Rp. ')}
            </Text>
            <View
              style={{
                backgroundColor: '#F6F8FF',
                height: 45,
                width: 150,
                marginBottom: 2,
                borderWidth: 0.4,
                borderColor: '#BB2427',
                borderRadius: 5,
              }}>
              <TextInput
                defaultValue={minPrice}
                placeholder="Harga"
                onChangeText={text => setMinPrice(text)}
                keyboardType="number-pad"></TextInput>
            </View>
          </View>

          <View>
            <Text
              style={{
                color: '#BB2427',
                marginBottom: 6,
                fontWeight: '600',
                fontSize: 12,
              }}>
              Harga Maksimal
            </Text>
            <Text
              style={{
                color: '#BB2427',
                marginBottom: 6,
                fontWeight: '600',
                fontSize: 12,
              }}>
              {convertCurrency(maxPrice, 'Rp. ')}
            </Text>
            <View
              style={{
                backgroundColor: '#F6F8FF',
                height: 45,
                width: 150,
                marginBottom: 2,
                borderWidth: 0.4,
                borderColor: '#BB2427',
                borderRadius: 5,
              }}>
              <TextInput
                defaultValue={maxPrice}
                placeholder="Harga"
                onChangeText={text => setMaxPrice(text)}
                keyboardType="number-pad"></TextInput>
            </View>
          </View>
        </View>
        <Text
          style={{
            color: '#BB2427',
            marginBottom: 12,
            fontWeight: '600',
            fontSize: 12,
            marginTop: 10,
          }}>
          Rating
        </Text>
        <View
          style={{
            backgroundColor: '#F6F8FF',
            height: 45,
            width: '100%',

            marginBottom: 2,
            borderWidth: 0.4,
            borderColor: '#BB2427',
            borderRadius: 5,
          }}>
          <TextInput
            defaultValue={rating}
            placeholder="Masukkan Rating"
            onChangeText={text => setRating(text)}
            keyboardType="number-pad"></TextInput>
        </View>
        <View style={styles.container}>
          <View>
            <Text
              style={{
                color: '#BB2427',
                marginBottom: 6,
                fontWeight: '600',
                fontSize: 12,
              }}>
              Buka
            </Text>
            <TouchableOpacity
              style={styles.input}
              onPress={() => showTimePicker('openingTime')}>
              <Text>{openingTime || 'Pilih Jam Buka'}</Text>
            </TouchableOpacity>
          </View>
          <View>
            <Text
              style={{
                color: '#BB2427',
                marginBottom: 6,
                fontWeight: '600',
                fontSize: 12,
              }}>
              Tutup
            </Text>
            <TouchableOpacity
              style={styles.input}
              onPress={() => showTimePicker('closingTime')}>
              <Text>{closingTime || 'Pilih Jam Tutup'}</Text>
            </TouchableOpacity>
          </View>

          <DateTimePickerModal
            isVisible={isTimePickerVisible}
            mode="time"
            onConfirm={handleConfirm}
            onCancel={hideTimePicker}
          />
        </View>
        <Text
          style={{
            color: '#BB2427',
            marginBottom: 12,
            fontWeight: '600',
            fontSize: 12,
            marginTop: 10,
          }}>
          Gambar
        </Text>
        <View
          style={{
            backgroundColor: '#F6F8FF',
            height: 45,
            width: '100%',

            marginBottom: 27,
            borderWidth: 0.4,
            borderColor: '#BB2427',
            borderRadius: 5,
          }}>
          <TextInput
            placeholder="Masukkan Alamat"
            onChangeText={text => setImgURL(text)}
          />
        </View>
        <View style={{marginBottom: 20}}>
          <RadioButton
            options={options}
            selectedOption={isOpen}
            onSelect={option => setIsOpen(option.value)}
          />
        </View>
        <TouchableOpacity
          style={styles.bottom}
          onPress={() => {
            if (route.params) {
              updateData();
            } else {
              tambahData();
            }
          }}>
          <Text
            style={{
              fontSize: 14,
              color: '#fff',
              fontWeight: '600',
            }}>
            {route.params ? 'Update' : 'Tambah'}
          </Text>
        </TouchableOpacity>
        {route.params ? (
          <TouchableOpacity
            style={[
              styles.bottom,
              {
                backgroundColor: 'white',
                borderColor: '#BB2427',
                borderWidth: 1,
              },
            ]}
            onPress={deleteData}>
            <Text
              style={{
                fontSize: 14,
                color: '#BB2427',
                fontWeight: '600',
              }}>
              Hapus Data
            </Text>
          </TouchableOpacity>
        ) : null}
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  bottom: {
    width: 250,
    height: 50,
    backgroundColor: '#BB2427',
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    borderWidth: 0.4,
    borderColor: '#BB2427',
    marginTop: 20,
    borderRadius: 5,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
    flexDirection: 'row',
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  input: {
    backgroundColor: '#F6F8FF',
    height: 45,
    width: 150,
    justifyContent: 'center',
    marginTop: 10,
    alignItems: 'center',
    borderWidth: 0.4,
    borderColor: '#BB2427',
    borderRadius: 5,
  },
});

export default TambahToko;
