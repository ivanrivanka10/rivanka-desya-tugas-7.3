import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {transcation} from '../data/data';

const DetailVoucher = ({navigation, route}) => {
  console.log(route.params);
  const dataVoucher = route.params;
  route.params;
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 56,
          elevation: 5,
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon
            name="arrowleft"
            size={20}
            color="#000000"
            style={{marginLeft: 25}}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontWeight: '700',
            color: 'black',
            fontSize: 18,
            marginLeft: 14,
          }}>
          Kode Promo
        </Text>
      </View>

      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 114,
          flexDirection: 'row',
          marginTop: 2,
          alignContent: 'center',
          alignSelf: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            height: 80,
            width: 80,
            borderRadius: 80,
            marginLeft: 35,
            marginTop: 1,
            backgroundColor: '#FFDFE0',
            alignItems: 'center',
            alignContent: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: 'black',
              fontWeight: 'bold',
              fontSize: 28,
              textShadowColor: '#BB2427',
              textShadowOffset: {width: -2, height: 2},
              textShadowRadius: 10,
            }}>
            {dataVoucher.logo}
          </Text>
        </View>
        <Text
          style={{
            fontWeight: '700',
            fontSize: 16,
            color: 'black',
            marginLeft: 30,
            width: 180,
          }}>
          {dataVoucher.title}
        </Text>
      </View>

      <View
        style={{
          width: '100%',
          height: '67%',
          backgroundColor: '#FFFFFF',
          marginTop: 7,
          marginBottom: 10,
          alignSelf: 'center',
        }}>
        <Text
          style={{
            marginLeft: 25,
            marginTop: 18,
            fontWeight: '700',
            fontSize: 14,
            color: 'black',
          }}>
          Ketentuan Promo
        </Text>
        <Text
          style={{
            fontSize: 14,
            color: 'black',
            width: '87%',
            textAlign: 'justify',
            marginLeft: 25,
            marginTop: 24,
          }}>
          {dataVoucher.detailVoucher}
        </Text>
      </View>
      <View
        style={{
          width: 335,
          height: 55,
          backgroundColor: '#BB2427',
          borderRadius: 9,
          alignSelf: 'center',
          marginTop: 10,
          alignItems: 'center',
          alignContent: 'center',
          justifyContent: 'center',
          marginBottom: 20,
        }}>
        <TouchableOpacity
          onPress={() => navigation.navigate('FormPemesanan', dataVoucher)}
          style={{
            width: 335,
            height: 55,
            alignItems: 'center',
            alignContent: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{color: 'white', fontWeight: '700', fontSize: 18}}>
            Gunakan Promo
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default DetailVoucher;
