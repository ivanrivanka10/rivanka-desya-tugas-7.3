import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';

import {voucher} from '../data/data';

const Voucher = ({navigation}) => {
  const RenderItem = ({item}) => (
    <View
      style={{
        width: 335,
        height: 119,
        borderRadius: 16,
        backgroundColor: 'white',
        marginTop: 10,
      }}>
      <TouchableOpacity
        onPress={() => navigation.navigate('DetailVoucher', item)}
        style={{
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
          height: 119,
          width: 335,
        }}>
        <View
          style={{
            height: 80,
            width: 80,
            borderRadius: 80,
            marginLeft: 15,
            marginTop: 1,
            backgroundColor: '#FFDFE0',
            alignItems: 'center',
            alignContent: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: 'black',
              fontWeight: 'bold',
              fontSize: 28,
              textShadowColor: '#BB2427',
              textShadowOffset: {width: -2, height: 2},
              textShadowRadius: 10,
            }}>
            {item.logo}
          </Text>
        </View>
        <View style={{marginLeft: 25}}>
          <Text
            style={{
              textAlign: 'left',
              width: 190,
              fontWeight: '700',
              fontSize: 14,
            }}>
            {item.title}
          </Text>
          <Text
            style={{
              textAlign: 'left',
              width: 190,
              marginTop: 7,
              marginBottom: 10,
              fontSize: 12,
            }}>
            {item.startDate} s/d {item.endDate}
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('FormPemesanan', item)}
            style={{
              width: 60,
              height: 20,
              alignSelf: 'flex-end',
            }}>
            <Text style={{textAlign: 'right', color: '#034262', fontSize: 12}}>
              Pakai Kupon
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    </View>
  );
  return (
    <View style={{flex: 1, alignItems: 'center', backgroundColor: '#F6F8FF'}}>
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 56,
          elevation: 5,
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon
            name="arrowleft"
            size={20}
            color="#000000"
            style={{marginLeft: 25}}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontWeight: '700',
            color: 'black',
            fontSize: 18,
            marginLeft: 14,
          }}>
          Voucher
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          height: 79,
          marginTop: 5,
          backgroundColor: 'white',
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: '55%',
            height: 45,
            borderColor: '#979797',
            borderWidth: 1,
            borderRadius: 10,
            marginLeft: 20,
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
          }}>
          <TextInput placeholder="Masukkan Kode Promo"></TextInput>
        </View>
        <TouchableOpacity>
          <View
            style={{
              height: 45,
              width: 112,
              backgroundColor: '#C03A2B',
              marginRight: 15,
              borderRadius: 10,
              alignContent: 'center',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{color: 'white', fontWeight: '700', fontSize: 12}}>
              Gunakan
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <ScrollView style={{width: '100%'}}>
        <View
          style={{alignContent: 'center', alignItems: 'center', width: '100%'}}>
          <FlatList
            data={voucher}
            renderItem={({item}) => <RenderItem item={item} />}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default Voucher;
