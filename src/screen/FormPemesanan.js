import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import moment from 'moment/moment';

const FormPemesanan = ({navigation, route}) => {
  const titleVoucher = route.params;
  const [shoesType, setShoesType] = useState('');
  const [size, setSize] = useState('');
  const [service, setService] = useState('Ganti Sol Sepatu');
  const [note, setNote] = useState('');
  const [color, setColor] = useState('');
  const {cartData} = useSelector(state => state.cart);
  const dispatch = useDispatch();
  const options = [
    {label: 'Ganti Sol Sepatu', value: 'Ganti Sol Sepatu'},
    {label: 'Jahit Sepatu', value: 'Jahit Sepatu'},
    {label: 'Repaint Sepatu', value: 'Repaint Sepatu'},
    {label: 'Cuci Sepatu', value: 'Cuci Sepatu'},
  ];

  const RadioButton = ({options, selectedOption, onSelect}) => {
    return (
      <View
        style={{
          justifyContent: 'space-evenly',
          marginLeft: 30,
        }}>
        {options.map(option => (
          <TouchableOpacity
            key={option.value}
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 16}}
            onPress={() => onSelect(option)}>
            <View
              style={{
                width: 20,
                height: 20,
                borderRadius: 2,
                borderWidth: 2,
                borderColor:
                  option.value === selectedOption ? '#BB2427' : '#B8B8B8',
                marginRight: 10,
                backgroundColor:
                  option.value === selectedOption ? '#BB2427' : 'white',
              }}></View>
            <Text>{option.label}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const tambahData = () => {
    var cartStore = [...cartData];
    const data = {
      id: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      shoesType: shoesType ? shoesType : 'Local Pride',
      size: size ? size : 'Unknown',
      color: color ? color : 'Unknown',
      note: note ? note : '-',
      service: service,
      time: new Date(),
      imageUrl:
        'https://images.tokopedia.net/img/cache/500-square/VqbcmM/2021/7/13/4275104f-10f7-4b6b-af66-201bccc420cb.jpg',
    };
    cartStore.push(data);
    dispatch({type: 'ADD_DATA_CART', data: cartStore});
    navigation.goBack();
  };
  return (
    <View style={{flex: 1, alignItems: 'center', backgroundColor: '#F6F8FF'}}>
      <ScrollView style={{width: '100%', height: '100%'}}>
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            height: 56,
            elevation: 5,
            flexDirection: 'row',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.pop()}>
            <Icon
              name="arrowleft"
              size={20}
              color="#000000"
              style={{marginLeft: 25}}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontWeight: '700',
              color: 'black',
              fontSize: 18,
              marginLeft: 14,
            }}>
            Form Pemesanan
          </Text>
        </View>
        <View style={{backgroundColor: 'white', height: '100%', width: '100%'}}>
          <Text
            style={{
              marginLeft: 30,
              color: '#BB2427',
              marginBottom: 12,
              fontWeight: '600',
              fontSize: 12,
            }}>
            Merek
          </Text>
          <View
            style={{
              backgroundColor: '#F6F8FF',
              height: 45,
              width: 335,
              alignSelf: 'center',
              marginBottom: 27,
            }}>
            <TextInput
              placeholder="Masukkan merek barang"
              onChangeText={text => setShoesType(text)}></TextInput>
          </View>
          <Text
            style={{
              marginLeft: 30,
              color: '#BB2427',
              marginBottom: 12,
              fontWeight: '600',
              fontSize: 12,
            }}>
            Warna
          </Text>
          <View
            style={{
              backgroundColor: '#F6F8FF',
              height: 45,
              width: 335,
              alignSelf: 'center',
              marginBottom: 27,
            }}>
            <TextInput
              placeholder="Warna Barang, cth : Merah - Putih "
              onChangeText={text => setColor(text)}></TextInput>
          </View>
          <Text
            style={{
              marginLeft: 30,
              color: '#BB2427',
              marginBottom: 12,
              fontWeight: '600',
              fontSize: 12,
            }}>
            Ukuran
          </Text>
          <View
            style={{
              backgroundColor: '#F6F8FF',
              height: 45,
              width: 335,
              alignSelf: 'center',
              marginBottom: 27,
            }}>
            <TextInput
              placeholder="Cth : S, M, L / 39,40"
              onChangeText={text => setSize(text)}></TextInput>
          </View>
          <Text
            style={{
              marginLeft: 30,
              color: '#BB2427',
              marginBottom: 12,
              fontWeight: '600',
              fontSize: 12,
            }}>
            Photo
          </Text>

          <View
            style={{
              width: 84,
              height: 84,
              borderColor: '#BB2427',
              borderWidth: 1,
              borderRadius: 10,
              marginLeft: 30,
              justifyContent: 'center',
              alignContent: 'center',
              alignItems: 'center',
              marginBottom: 20,
            }}>
            <Icon name="camerao" size={24} color={'#BB2427'}></Icon>
            <Text style={{color: '#BB2427', fontWeight: '300', marginTop: 5}}>
              Add Photo
            </Text>
          </View>
          <View style={{}}>
            <RadioButton
              options={options}
              selectedOption={service}
              onSelect={option => setService(option.value)}
            />
          </View>
          <Text
            style={{
              marginLeft: 30,
              color: '#BB2427',
              marginBottom: 12,
              marginTop: 24,
              fontWeight: '600',
              fontSize: 12,
            }}>
            Catatan
          </Text>
          <View
            style={{
              backgroundColor: '#F6F8FF',
              height: 92,
              width: '85%',
              alignSelf: 'center',
              marginBottom: 27,
            }}>
            <TextInput
              placeholder="Cth : ingin ganti sol baru"
              onChangeText={text => setNote(text)}
              style={{width: 300, height: 100}}></TextInput>
          </View>
          <Text
            style={{
              marginLeft: 30,
              color: '#BB2427',
              marginBottom: 12,
              marginTop: 14,
              fontWeight: '600',
              fontSize: 12,
            }}>
            Kupon Promo
          </Text>
          <TouchableOpacity
            style={{
              width: 335,
              height: 47,
              borderRadius: 8,
              backgroundColor: titleVoucher ? '#0342621A' : 'white',
              borderColor: titleVoucher ? '#034262' : 'red',
              borderWidth: 1,
              marginLeft: 30,
              marginBottom: 34,
            }}
            onPress={() => navigation.navigate('Voucher')}>
            <View
              style={{
                width: 335,
                height: 47,
                justifyContent: 'space-between',
                flexDirection: 'row',
                alignContent: 'center',
                alignItems: 'center',
              }}>
              <View style={{flexDirection: 'row'}}>
                <Text
                  style={{
                    fontWeight: '300',
                    fontSize: 12,
                    color: 'black',
                    marginLeft: 17,
                  }}>
                  {titleVoucher ? titleVoucher.title : 'Pilih Kupon Promo'}
                </Text>
                {titleVoucher ? (
                  <Icon
                    name="checkcircle"
                    style={{marginLeft: 5}}
                    color={'#11A84E'}></Icon>
                ) : (
                  <Icon
                    name="checkcircle"
                    style={{marginRight: 17}}
                    color={'white'}></Icon>
                )}
              </View>
              {titleVoucher ? (
                <Icon
                  name="close"
                  style={{marginRight: 17}}
                  color={'#2E3A59'}></Icon>
              ) : (
                <Icon
                  name="right"
                  style={{marginRight: 17}}
                  color={'#C03A2B'}></Icon>
              )}
            </View>
          </TouchableOpacity>
          <View
            style={{
              height: 55,
              width: 335,
              backgroundColor: '#BB2427',
              borderRadius: 10,
              alignSelf: 'center',
              marginBottom: 48,
              alignContent: 'center',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              onPress={tambahData}
              style={{
                height: '100%',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: '700',
                  color: 'white',
                }}>
                Pesan Sekarang
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default FormPemesanan;
