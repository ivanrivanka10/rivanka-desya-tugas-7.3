import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Rating} from 'react-native-stock-star-rating';
import {useDispatch, useSelector} from 'react-redux';

const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const {storeData} = useSelector(state => state.home);
  const {loginData, isLoggedIn} = useSelector(state => state.login);
  console.log('test', loginData);
  console.log('login userhome', isLoggedIn);
  const RenderItem = ({item}) => (
    <TouchableOpacity
      style={[styles.containertoko]}
      onPress={() => navigation.navigate('DetailProduct', item)}>
      <Image
        source={{uri: item.imageUrl}}
        style={{
          margin: 6,
          marginRight: 19,
          height: 121,
          width: 80,
          borderRadius: 5,
        }}></Image>
      <View
        style={{
          flexDirection: 'column',
          width: '65%',
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 15,
            alignItems: 'center',
          }}>
          <Rating stars={item.rating} maxStars={5} size={15} />

          {item.favourite ? (
            <AntDesign
              name="heart"
              color={'red'}
              style={{
                marginLeft: 14,
              }}></AntDesign>
          ) : (
            <AntDesign
              name="hearto"
              color={'#ADADAD'}
              style={{
                marginLeft: 14,
              }}></AntDesign>
          )}
        </View>
        <Text
          style={{
            color: '#D8D8D8',
            fontWeight: '500',
            fontSize: 10,
          }}>
          {item.rating} Ratings
        </Text>
        <Text
          style={{
            marginTop: 4.5,
            marginBottom: 4,
            color: '#201F26',
            fontSize: 12,
            fontWeight: '600',
          }}>
          {item.title}
        </Text>
        <Text
          style={{
            color: '#D8D8D8',
            fontWeight: '500',
            fontSize: 10,
          }}>
          {item.address}
        </Text>

        {item.isOpen ? (
          <View
            style={{
              backgroundColor: '#11A84E1F',
              width: 58,
              height: 21,
              borderRadius: 10.5,
              marginTop: 16,
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#11A84E',
                fontSize: 12,
                fontWeight: '700',
                alignSelf: 'center',
                marginTop: 2,
              }}>
              BUKA
            </Text>
          </View>
        ) : (
          <View
            style={{
              backgroundColor: '#E64C3C33',
              width: 58,
              height: 21,
              borderRadius: 10.5,
              marginTop: 16,
              alignItems: 'center',
            }}>
            <Text
              style={{
                color: '#EA3D3D',
                fontSize: 12,
                fontWeight: '700',
                alignSelf: 'center',
                marginTop: 2,
              }}>
              TUTUP
            </Text>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF'}}>
      <ScrollView>
        <View style={styles.rectanglehome}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 56,
              marginLeft: 22,
            }}>
            <Image
              source={{uri: loginData.imgProfile}}
              style={styles.photoprofil}></Image>
          </View>
          <Text style={styles.sayhello}>Hello, {loginData.nama}!</Text>
          <Text style={styles.jargon}>Ingin merawat dan perbaiki</Text>
          <Text style={styles.jargon}>sepatumu? cari disini</Text>
          <View style={{flexDirection: 'row', marginTop: 20, marginLeft: 22}}>
            <View style={styles.searchcontainer}>
              <Image
                source={require('../assets/icon/Search.png')}
                style={styles.searchicon}></Image>
            </View>
            <View style={styles.filtercontainer}>
              <Image
                source={require('../assets/icon/Filter.png')}
                style={styles.searchicon}></Image>
            </View>
          </View>
        </View>
        <ScrollView style={styles.rectanglehomescroll}>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 18,
              alignItems: 'center',
              alignContent: 'center',
              justifyContent: 'center',
            }}>
            <View style={styles.boxcategory}>
              <TouchableOpacity>
                <Image
                  source={require('../assets/icon/sepatu.png')}
                  style={styles.iconcategory}></Image>
                <Text style={styles.texticoncategory}>Sepatu</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.boxcategory}>
              <Image
                source={require('../assets/icon/jaket.png')}
                style={styles.iconcategory}></Image>
              <Text style={styles.texticoncategory}>Jaket</Text>
            </View>
            <View style={styles.boxcategory}>
              <Image
                source={require('../assets/icon/tas.png')}
                style={styles.iconcategory}></Image>
              <Text style={styles.texticoncategory}>Tas</Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginLeft: 22,
              marginRight: 30,
              marginTop: 22,
              marginBottom: 25,
            }}>
            <Text style={{fontWeight: '600', color: '#0A0827', fontSize: 12}}>
              Rekomendasi Terdekat
            </Text>

            <Text
              style={{
                color: '#E64C3C',
                fontWeight: '500',
                fontSize: 10,
              }}>
              View All
            </Text>
          </View>

          <FlatList
            data={storeData}
            renderItem={({item}) => <RenderItem item={item} />}
          />
        </ScrollView>
      </ScrollView>
      <View
        style={{
          position: 'absolute',
          bottom: 30,
          right: 30,
          width: 35,
          height: 35,
          borderRadius: 20,
          backgroundColor: '#E64C3C',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('TambahToko')}>
          <AntDesign name="plus" color={'white'} size={20} />
        </TouchableOpacity>
      </View>
      <View
        style={{
          position: 'absolute',
          bottom: 30,
          left: 30,
          width: 35,
          height: 35,
          borderRadius: 20,
          backgroundColor: '#E64C3C',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={() => dispatch({type: 'RESET_DATA'})}>
          <AntDesign name="minus" color={'white'} size={20} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containertoko: {
    width: '87%',
    height: 133,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom: 5,
    flexDirection: 'row',
  },
  rectanglehome: {
    width: '100%',
    height: 295,
    backgroundColor: '#FFFFFF',
  },
  iconcategory: {
    width: 45,
    height: 45,
    alignSelf: 'center',
  },
  texticoncategory: {
    fontWeight: '600',
    color: '#BB2427',
    fontSize: 12,
    textAlign: 'center',
    marginTop: 9,
  },
  boxcategory: {
    width: 95,
    height: 95,
    backgroundColor: 'white',
    borderRadius: 16.3,
    alignSelf: 'center',
    marginHorizontal: 15,
    justifyContent: 'center',
  },
  filtercontainer: {
    height: 45,
    width: 45,
    marginLeft: 15,
    backgroundColor: '#F6F8FF',
    borderRadius: 14,
  },
  searchcontainer: {
    height: 45,
    width: '78%',
    backgroundColor: '#F6F8FF',
    borderRadius: 14,
  },
  rectanglehomescroll: {
    width: '100%',
    height: '100%',
    backgroundColor: '#F6F8FF',
  },
  photoprofil: {
    width: 50,
    height: 50,
    borderRadius: 10,
  },
  bagicon: {
    width: 25,
    height: 25,
    alignSelf: 'center',
    marginRight: 27,
    alignSelf: 'flex-end',
  },
  searchicon: {
    width: 16,
    height: 16,
    marginLeft: 16,
    textAlignVertical: 'center',
    marginTop: 15,
  },
  sayhello: {
    marginLeft: 22,
    fontSize: 15,
    fontWeight: '500',
    height: 36,
    marginTop: 5,
    color: '#034262',
  },
  jargon: {
    fontWeight: '700',
    fontSize: 20,
    color: '#0A0827',
    marginLeft: 22,
    marginTop: 5,
  },
});

export default Home;
