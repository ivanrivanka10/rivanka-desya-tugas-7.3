import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';

const CheckoutList = ({navigation, item}) => {
  return (
    <View
      style={{
        width: 353,
        height: 120,
        marginLeft: 10,
        backgroundColor: 'white',
        marginHorizontal: 20,
        flexDirection: 'row',
      }}>
      <Image
        source={{uri: item.imageUrl}}
        style={{
          height: 84,
          width: 84,
          marginLeft: 14,
          marginTop: 24,
          marginBottom: 27,
          borderRadius: 9,
        }}
      />
      <View style={{flexDirection: 'column', marginTop: 33, marginLeft: 13}}>
        <Text
          style={{
            marginLeft: 10,
            fontWeight: '500',
            fontSize: 12,
            color: '#201F26',
          }}>
          {item.shoesType} - {item.color} - {item.size}
        </Text>
        <Text
          style={{
            marginLeft: 10,
            fontWeight: '400',
            marginTop: 11,
            fontSize: 12,
            color: '#737373',
          }}>
          {item.service}
        </Text>
        <Text
          style={{
            marginLeft: 10,
            fontWeight: '400',
            marginTop: 11,
            fontSize: 12,
            color: '#737373',
          }}>
          Note : {item.note}
        </Text>
      </View>
    </View>
  );
};
export default CheckoutList;
