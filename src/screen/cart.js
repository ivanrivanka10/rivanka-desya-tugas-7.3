import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import CartList from './cartList';
import {useDispatch, useSelector} from 'react-redux';

const Cart = ({navigation, route}) => {
  const dataToko = route.params;
  const dispatch = useDispatch();
  const {cartData} = useSelector(state => state.cart);
  return (
    <View style={{flex: 1, alignItems: 'center', backgroundColor: '#F6F8FF'}}>
      <ScrollView
        style={{
          width: '100%',
        }}>
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            height: 56,
            elevation: 5,
            flexDirection: 'row',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.pop()}>
            <Icon
              name="arrowleft"
              size={20}
              color="#000000"
              style={{marginLeft: 25}}
            />
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '85%',
            }}>
            <Text
              style={{
                fontWeight: '700',
                color: 'black',
                fontSize: 18,
                marginLeft: 14,
              }}>
              Keranjang
            </Text>
            <TouchableOpacity
              onPress={() => dispatch({type: 'RESET_DATA_CART'})}>
              <Icon
                name="delete"
                size={20}
                color="#000000"
                style={{marginLeft: 25}}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{height: 10}}></View>

        <FlatList
          data={cartData}
          renderItem={({item}) => (
            <CartList item={item} navigation={navigation} />
          )}
        />
        <View style={{height: 30}}></View>
        <View
          style={{
            width: 190,
            height: 30,
            marginBottom: 50,
            alignSelf: 'center',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => navigation.navigate('FormPemesanan')}>
            <View
              style={{
                flexDirection: 'row',
                alignContent: 'center',
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              <Icon
                name="plussquareo"
                style={{marginRight: 9}}
                size={24}
                color={'#BB2427'}
              />
              <Text style={{fontWeight: '700', fontSize: 14, color: '#BB2427'}}>
                Tambah Barang
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View
          style={{
            height: 55,
            width: 335,
            backgroundColor: '#BB2427',
            borderRadius: 10,
            alignSelf: 'center',
            marginBottom: 48,
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <TouchableOpacity
            onPress={() => {
              if (cartData != '') {
                navigation.navigate('CheckOut', dataToko);
              }
            }}
            style={{
              height: '100%',
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: '700',
                color: 'white',
              }}>
              Selanjutnya
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Cart;
