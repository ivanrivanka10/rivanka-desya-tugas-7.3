import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';

import CheckoutList from './CheckOutList';
import moment from 'moment/moment';

const CheckOut = ({navigation, route}) => {
  const dataToko = route.params;
  const dispatch = useDispatch();
  const {cartData} = useSelector(state => state.cart);
  const {transactionData} = useSelector(state => state.transaction);
  const {loginData} = useSelector(state => state.login);

  const tambahData = () => {
    var transactionStore = [...transactionData];
    const data = {
      idTransaction: moment(new Date()).format('YYYYMMDDHHmmssSSS'),
      reservationCode: moment(new Date()).format('DDHHmmss'),
      date: moment(new Date()).format('MMMM DD, YYYY'),
      customerData: loginData,
      storeData: dataToko,
      cartData: cartData,
    };
    transactionStore.push(data);
    console.log('data transaksi', transactionStore);
    dispatch({type: 'ADD_DATA_TRANSACTION', data: transactionStore});
    dispatch({type: 'RESET_DATA_CART', data: transactionStore});
    console.log('test', transactionStore);
    navigation.navigate('SuccesPayment');
  };

  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <ScrollView style={{height: '100%'}}>
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            height: 56,
            elevation: 5,
            flexDirection: 'row',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.pop()}>
            <Icon
              name="arrowleft"
              size={20}
              color="#000000"
              style={{marginLeft: 25}}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontWeight: '700',
              color: 'black',
              fontSize: 18,
              marginLeft: 14,
            }}>
            {' '}
            Checkout
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            height: 153,
            backgroundColor: 'white',
            marginTop: 6,
          }}>
          <Text
            style={{
              marginLeft: 26,
              fontWeight: '500',
              fontSize: 14,
              color: '#979797',
              marginTop: 20,
            }}>
            Data Customer
          </Text>
          <Text
            style={{
              marginLeft: 26,
              fontWeight: '400',
              fontSize: 14,
              color: 'black',
              marginTop: 6,
            }}>
            {loginData.nama} - {loginData.phoneNumber}
          </Text>
          <Text
            style={{
              marginLeft: 26,
              fontWeight: '400',
              fontSize: 14,
              color: 'black',
              marginTop: 6,
            }}>
            {loginData.address}
          </Text>
          <Text
            style={{
              marginLeft: 26,
              fontWeight: '400',
              fontSize: 14,
              color: 'black',
              marginTop: 6,
            }}>
            {loginData.email}
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            height: 103,
            backgroundColor: 'white',
            marginTop: 6,
          }}>
          <Text
            style={{
              marginLeft: 26,
              fontWeight: '500',
              fontSize: 14,
              color: '#979797',
              marginTop: 20,
            }}>
            Alamat Outlet Tujuan
          </Text>

          <Text
            style={{
              marginLeft: 26,
              fontWeight: '400',
              fontSize: 14,
              color: 'black',
              marginTop: 6,
            }}>
            {dataToko.title} (0274) 1234
          </Text>
          <Text
            style={{
              marginLeft: 26,
              fontWeight: '400',
              fontSize: 14,
              color: 'black',
              marginTop: 6,
            }}>
            {dataToko.address}
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            backgroundColor: 'white',
            marginTop: 4,
          }}>
          <Text
            style={{
              marginLeft: 26,
              fontWeight: '500',
              fontSize: 14,
              color: '#979797',
              marginTop: 20,
            }}>
            Barang
          </Text>
          <FlatList
            data={cartData}
            renderItem={({item}) => (
              <CheckoutList item={item} navigation={navigation} />
            )}
          />
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 28,
              marginTop: 16,
              marginBottom: 10,
              justifyContent: 'space-between',
            }}>
            <Text style={{fontWeight: '400', fontSize: 16, color: 'black'}}>
              1 Pasang
            </Text>
            <Text
              style={{
                marginRight: 21,
                fontWeight: '700',
                color: 'black',
                fontSize: 16,
              }}>
              @Rp 50.000
            </Text>
          </View>
        </View>
        <View
          style={{
            width: '100%',
            height: 124,
            backgroundColor: 'white',
            marginTop: 5,
            paddingHorizontal: 27,
          }}>
          <Text
            style={{
              marginTop: 8,
              fontSize: 14,
              fontWeight: '500',
              color: 'rgba(151, 151, 151, 1)',
            }}>
            Rician Pembayaran
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 11,
            }}>
            <Text style={{width: 80}}>Cuci Sepatu</Text>
            <Text
              style={{
                width: 200,
                color: 'rgba(255, 193, 7, 1)',
                marginLeft: -10,
              }}>
              x1 kali
            </Text>
            <Text>Rp. 30.000</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 11,
            }}>
            <Text style={{width: 80}}>Biaya Antar</Text>
            <Text>Rp. 3.000</Text>
          </View>
          <View
            style={{
              width: 327,
              height: 1,
              backgroundColor: 'rgba(237, 237, 237, 1)',
              marginTop: 10,
              alignSelf: 'center',
            }}></View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 8,
            }}>
            <Text style={{width: 80}}>Total Antar</Text>
            <Text
              style={{
                fontWeight: '700',
                fontSize: 12,
                color: 'rgba(3, 66, 98, 1)',
              }}>
              Rp. 33.000
            </Text>
          </View>
        </View>
        <View
          style={{
            width: '100%',
            height: 154,
            backgroundColor: 'white',
            marginTop: 5,
            marginBottom: 30,
          }}>
          <Text style={{marginLeft: 27, marginTop: 16}}>Pilih Pembayaran</Text>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection: 'row', marginTop: 16}}>
              <View
                style={{
                  width: 126,
                  height: 82,
                  borderRadius: 4,
                  borderWidth: 1,
                  borderColor: 'rgba(225, 225, 225, 1)',
                  margin: 5,
                  alignContent: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  style={{
                    alignContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 82,
                    width: 126,
                  }}>
                  <Icon name="bank" size={30} color={'rgba(0, 0, 0, 1)'} />
                  <Text
                    style={{
                      fontWeight: '400',
                      color: 'black',
                      fontSize: 12,
                      marginTop: 5,
                    }}>
                    Bank Transfer
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: 126,
                  height: 82,
                  borderRadius: 4,
                  borderWidth: 1,
                  borderColor: 'rgba(225, 225, 225, 1)',
                  margin: 5,
                  alignContent: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  style={{
                    alignContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 82,
                    width: 126,
                  }}>
                  <Image
                    style={{width: 75, height: 23}}
                    source={{
                      uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Logo_ovo_purple.svg/2560px-Logo_ovo_purple.svg.png',
                    }}
                  />
                  <Text
                    style={{
                      fontWeight: '400',
                      color: 'black',
                      fontSize: 12,
                      marginTop: 5,
                    }}>
                    OVO
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  width: 126,
                  height: 82,
                  borderRadius: 4,
                  borderWidth: 1,
                  borderColor: 'rgba(225, 225, 225, 1)',
                  margin: 5,
                  alignContent: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  style={{
                    alignContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 82,
                    width: 126,
                  }}>
                  <Icon
                    name="creditcard"
                    size={30}
                    color={'rgba(0, 0, 0, 1)'}
                  />
                  <Text
                    style={{
                      fontWeight: '400',
                      color: 'black',
                      fontSize: 12,
                      marginTop: 5,
                    }}>
                    Kartu Kredit
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
        <View
          style={{
            backgroundColor: '#BB2427',
            height: 55,
            width: 335,
            marginBottom: 20,
            alignSelf: 'center',
            borderRadius: 10,
          }}>
          <TouchableOpacity
            onPress={tambahData}
            style={{
              alignContent: 'center',
              alignItems: 'center',
              justifyContent: 'center',
              height: 55,
              width: 335,
            }}>
            <Text style={{fontWeight: '700', fontSize: 16, color: 'white'}}>
              Pesan Sekarang
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default CheckOut;
