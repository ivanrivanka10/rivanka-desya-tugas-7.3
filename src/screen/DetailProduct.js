import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Rating} from 'react-native-stock-star-rating';
import {createOpenLink} from 'react-native-open-maps';
import {convertCurrency} from '../utils/Helpers';
const DetailProduct = ({navigation, route}) => {
  console.log(route.params);
  const dataDetail = route.params;
  const yosemite = {
    latitude: dataDetail.latitude,
    longitude: dataDetail.longitude,
  };
  const openYosemite = createOpenLink(yosemite);
  const openYosemiteZoomedOut = createOpenLink({...yosemite, zoom: 30});
  return (
    <View style={{backgroundColor: 'white', flex: 1}}>
      <ScrollView>
        <Image
          source={{uri: dataDetail.imageUrl}}
          style={{width: '100%', height: 316}}></Image>
        <View
          style={{
            height: 316,
            flexDirection: 'row',
            width: '100%',

            borderRadius: 20,
            marginTop: -316,
          }}>
          <TouchableOpacity onPress={() => navigation.pop()}>
            <AntDesign
              name="arrowleft"
              color={'white'}
              size={20}
              style={{margin: 25}}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('Cart', dataDetail)}
            style={{position: 'absolute', right: 27}}>
            <FontAwesome
              name="shopping-bag"
              color={'white'}
              size={20}
              style={{marginTop: 25}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            height: '100%',
            borderRadius: 20,
            marginTop: -25,
          }}>
          <Text
            style={{
              fontWeight: '700',
              fontSize: 18,
              color: '#201F26',
              marginTop: 24,
              marginLeft: 33,
            }}>
            {dataDetail.title}
          </Text>
          <View style={{marginLeft: 33}}>
            <Rating stars={dataDetail.rating} maxStars={5} size={15} />
          </View>

          <View
            style={{
              flexDirection: 'row',
              marginTop: 10,
              marginLeft: 33,
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <FontAwesome
              name="map-marker"
              color={'#BB2427'}
              size={18}
              style={{marginRight: 12}}
            />
            <Text
              style={{
                width: '70%',
                fontSize: 10,
                fontWeight: '400',
                color: '#979797',
              }}>
              {dataDetail.address}
            </Text>
            <TouchableOpacity
              onPress={openYosemite}
              style={{marginRight: 14, right: 0, position: 'absolute'}}>
              <Text
                style={{
                  color: '#3471CD',

                  fontWeight: '700',
                  fontSize: 12,
                }}>
                Lihat Maps
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginLeft: 30,
              flexDirection: 'row',
              alignContent: 'center',
              alignItems: 'center',
              marginTop: 13,
            }}>
            {dataDetail.isOpen ? (
              <View
                style={{
                  backgroundColor: '#11A84E1F',
                  width: 58,
                  height: 21,
                  borderRadius: 10.5,

                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#11A84E',
                    fontSize: 12,
                    fontWeight: '700',
                    alignSelf: 'center',
                    marginTop: 2,
                  }}>
                  BUKA
                </Text>
              </View>
            ) : (
              <View
                style={{
                  backgroundColor: '#E64C3C33',
                  width: 58,
                  height: 21,
                  borderRadius: 10.5,

                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: '#EA3D3D',
                    fontSize: 12,
                    fontWeight: '700',
                    alignSelf: 'center',
                    marginTop: 2,
                  }}>
                  TUTUP
                </Text>
              </View>
            )}
            <Text
              style={{
                fontWeight: '700',
                fontSize: 12,
                marginLeft: 15,
                color: '#343434',
              }}>
              {dataDetail.openingTime}
            </Text>
            <Text
              style={{
                fontWeight: '700',
                fontSize: 12,
                marginLeft: 15,
                color: '#343434',
              }}>
              -
            </Text>
            <Text
              style={{
                fontWeight: '700',
                fontSize: 12,
                marginLeft: 15,
                color: '#343434',
              }}>
              {dataDetail.closingTime}
            </Text>
          </View>
          <View
            style={{
              width: '100%',
              height: 1,
              backgroundColor: '#EEEEEE',
              marginTop: 13,
            }}></View>
          <Text
            style={{
              marginTop: 25,
              marginLeft: 32,
              fontWeight: '500',
              fontSize: 16,
              color: '#201F26',
            }}>
            Deskripsi
          </Text>
          <Text
            style={{
              marginTop: 10,
              marginLeft: 32,
              fontWeight: '400',
              fontSize: 14,
              color: '#595959',
              width: 323,
              textAlign: 'justify',
            }}>
            {dataDetail.description}
          </Text>
          <Text
            style={{
              marginTop: 23,
              marginLeft: 32,
              fontWeight: '500',
              fontSize: 16,
              color: '#201F26',
            }}>
            Range Biaya
          </Text>
          <Text
            style={{
              marginTop: 6,
              marginLeft: 32,
              fontWeight: '500',
              fontSize: 16,
              color: '#8D8D8D',
            }}>
            {convertCurrency(dataDetail.minPrice, 'Rp.')} -{' '}
            {convertCurrency(dataDetail.maxPrice, 'Rp.')}
          </Text>
          <TouchableOpacity
            onPress={() => navigation.navigate('FormPemesanan')}>
            <View
              style={{
                backgroundColor: '#BB2427',
                width: 335,
                height: 55,
                alignSelf: 'center',
                alignContent: 'center',
                justifyContent: 'center',
                marginTop: 50,

                borderRadius: 10,
              }}>
              <Text
                style={{
                  alignSelf: 'center',
                  color: '#FFFFFF',
                  fontWeight: '700',
                  fontSize: 16,
                }}>
                Repair Disini
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => navigation.navigate('TambahToko', dataDetail)}>
            <View
              style={{
                backgroundColor: 'white',
                borderColor: '#BB2427',
                borderWidth: 1,
                width: 335,
                height: 55,
                alignSelf: 'center',
                alignContent: 'center',
                justifyContent: 'center',
                marginTop: 20,

                borderRadius: 10,
              }}>
              <Text
                style={{
                  alignSelf: 'center',
                  color: '#BB2427',
                  fontWeight: '700',
                  fontSize: 16,
                }}>
                Edit Toko
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default DetailProduct;
