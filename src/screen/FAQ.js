import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {faq, transcation} from '../data/data';
import TransactionList from './transactionList';
import CartList from './cartList';

const FAQ = ({navigation}) => {
  const RenderItem = ({item}) => {
    const [showAnswer, setShowAnswer] = useState(false);
    return (
      <View style={{marginTop: 10}}>
        <TouchableOpacity onPress={() => setShowAnswer(!showAnswer)}>
          <View
            style={{
              width: 363,
              height: 68,
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 20,
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontWeight: '500', color: '#000000', fontSize: 14}}>
              {item.subText}
            </Text>
            <Icon name={showAnswer ? 'up' : 'down'} size={15}></Icon>
          </View>
        </TouchableOpacity>
        {showAnswer && (
          <View
            style={{
              width: 363,
              height: 68,
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 20,
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontWeight: '500', color: '#595959', fontSize: 14}}>
              {item.text}
            </Text>
          </View>
        )}
      </View>
    );
  };

  return (
    <View style={{flex: 1, alignItems: 'center', backgroundColor: '#F6F8FF'}}>
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 56,
          elevation: 5,
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon
            name="arrowleft"
            size={20}
            color="#000000"
            style={{marginLeft: 25}}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontWeight: '700',
            color: 'black',
            fontSize: 18,
            marginLeft: 14,
          }}>
          FAQ
        </Text>
      </View>

      <FlatList
        data={faq}
        renderItem={({item}) => <RenderItem item={item} />}
      />
    </View>
  );
};

export default FAQ;
