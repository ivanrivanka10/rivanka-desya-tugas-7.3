import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {transcation} from '../data/data';

const SuccesPayment = ({navigation}) => {
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <TouchableOpacity
        onPress={() => navigation.navigate('bottom')}
        style={{width: 24, height: 24, marginLeft: 21, marginTop: 27}}>
        <Icon name="close" size={24} />
      </TouchableOpacity>

      <Text
        style={{
          color: '#11A84E',
          marginTop: 135,
          fontWeight: '700',
          fontSize: 20,
          textAlign: 'center',
        }}>
        Reservasi Berhasil
      </Text>

      <View
        style={{
          width: 133,
          height: 133,
          borderRadius: 50,
          backgroundColor: 'rgba(17, 168, 78, 1)',
          opacity: 0.4,
          alignContent: 'center',
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 53,
        }}>
        <Icon name="check" size={80} color={'rgba(17, 168, 78, 1)'} />
      </View>

      <Text
        style={{
          fontWeight: '400',
          width: 300,
          textAlign: 'center',
          alignSelf: 'center',
          marginTop: 64,
          fontSize: 18,
          color: 'black',
        }}>
        Kami Telah Mengirimkan Kode Reservasi ke Menu Transaksi
      </Text>

      <View
        style={{
          width: 335,
          backgroundColor: 'rgba(187, 36, 39, 1)',
          height: 55,
          bottom: 20,
          position: 'absolute',
          alignSelf: 'center',
          borderRadius: 10,
          alignItems: 'center',
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.navigate('bottom')}
          style={{
            height: '100%',
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={{fontWeight: '700', color: 'white', fontSize: 16}}>
            Lihat Kode Reservasi
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default SuccesPayment;
