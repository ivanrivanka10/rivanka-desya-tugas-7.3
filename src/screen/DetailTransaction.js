import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {transcation} from '../data/data';

const DetailTransaction = ({navigation, route}) => {
  console.log(route.params);
  const dataTransaction = route.params;
  route.params;

  const RenderItem = ({item}) => (
    console.log('ini apaan', item),
    (
      <View
        style={{
          width: '97%',
          height: 135,
          backgroundColor: '#FFFFFF',
          marginTop: 5,
          borderRadius: 8,
          marginBottom: 10,
          alignSelf: 'center',
          flexDirection: 'row',
        }}>
        <Image
          source={{uri: item.imageUrl}}
          style={{
            height: 84,
            width: 84,
            marginLeft: 14,
            marginTop: 24,
            marginBottom: 27,
            borderRadius: 9,
          }}
        />
        <View style={{flexDirection: 'column', marginTop: 33, marginLeft: 13}}>
          <Text
            style={{
              marginLeft: 10,
              fontWeight: '500',
              fontSize: 14,
              color: '#201F26',
            }}>
            {item.shoesType} - {item.color} - {item.size}
          </Text>
          <Text
            style={{
              marginLeft: 10,
              fontWeight: '400',
              marginTop: 11,
              fontSize: 14,
              color: '#737373',
            }}>
            {item.service}
          </Text>
          <Text
            style={{
              marginLeft: 10,
              fontWeight: '400',
              marginTop: 11,
              fontSize: 14,
              color: '#737373',
            }}>
            Note : -
          </Text>
        </View>
      </View>
    )
  );
  return (
    <View style={{backgroundColor: '#F6F8FF', flex: 1}}>
      <ScrollView>
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            height: 56,
            elevation: 5,
            flexDirection: 'row',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.pop()}>
            <Icon
              name="arrowleft"
              size={20}
              color="#000000"
              style={{marginLeft: 25}}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontWeight: '700',
              color: 'black',
              fontSize: 18,
              marginLeft: 14,
            }}></Text>
        </View>
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            height: 238,
            marginTop: 2,
          }}>
          <Text
            style={{
              textAlign: 'center',
              marginTop: 16,
              fontWeight: '500',
              fontSize: 12,
              color: '#BDBDBD',
            }}>
            {dataTransaction.date} {dataTransaction.time}
          </Text>

          <Text
            style={{
              textAlign: 'center',
              fontWeight: '700',
              marginTop: 37,
              fontSize: 36,
              color: '#201F26',
            }}>
            C{dataTransaction.reservationCode}
          </Text>
          <Text
            style={{
              fontWeight: '400',
              marginTop: 13,
              fontSize: 14,
              textAlign: 'center',
            }}>
            Kode Reservasi
          </Text>

          <Text
            style={{
              fontSize: 16,
              fontWeight: '400',
              textAlign: 'center',
              marginTop: 42,
              width: 190,
              alignSelf: 'center',
            }}>
            Sebutkan Kode Reservasi saat tiba di outlet
          </Text>
        </View>
        <Text
          style={{
            marginLeft: 6,
            fontWeight: '400',
            fontSize: 14,
            color: 'black',
            marginVertical: 18,
          }}>
          Barang
        </Text>
        <FlatList
          data={dataTransaction.cartData}
          renderItem={({item}) => <RenderItem item={item} />}
        />

        <Text
          style={{
            marginLeft: 6,
            fontWeight: '400',
            fontSize: 14,
            color: 'black',
            marginVertical: 18,
            marginTop: 20,
          }}>
          Status Pesanan
        </Text>
        <View
          style={{
            width: '97%',
            height: 80,
            backgroundColor: '#FFFFFF',
            marginTop: 5,
            borderRadius: 8,
            marginBottom: 10,
            alignSelf: 'center',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <View>
            <View
              style={{
                width: 15,
                height: 15,
                backgroundColor:
                  dataTransaction.detail == 'Canceled' ? 'red' : 'green',
                borderRadius: 24,
                marginLeft: 20,
              }}></View>
          </View>

          <View style={{flexDirection: 'column', width: '70%'}}>
            <Text
              style={{
                marginLeft: 10,
                fontWeight: '500',
                fontSize: 14,
                color: 'black',
              }}>
              {dataTransaction.detail == 'Canceled'
                ? 'Gagal Reservasi'
                : 'Telah Reservasi'}
            </Text>
            <Text
              style={{
                marginLeft: 10,
                fontWeight: '400',
                marginTop: 11,
                fontSize: 14,
                color: '#737373',
              }}>
              {dataTransaction.date}
            </Text>
          </View>
          <Text
            style={{
              marginLeft: 10,
              fontWeight: '400',
              fontSize: 14,
              color: '#737373',
              textAlign: 'right',
              marginRight: 10,
            }}>
            {dataTransaction.time}
          </Text>
        </View>
      </ScrollView>
    </View>
  );
};
export default DetailTransaction;
