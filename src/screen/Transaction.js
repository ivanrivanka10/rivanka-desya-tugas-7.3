import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {transcation} from '../data/data';
import TransactionList from './transactionList';
import {useDispatch, useSelector} from 'react-redux';

const Transaction = ({navigation}) => {
  const dispatch = useDispatch();
  const {transactionData} = useSelector(state => state.transaction);
  console.log('data trasn', transactionData);
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF', alignItems: 'center'}}>
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 56,
          elevation: 5,
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontWeight: '700',
            color: 'black',
            fontSize: 18,
            marginLeft: 24,
          }}>
          Transaksi
        </Text>
      </View>
      <View style={{height: 20}}></View>
      <FlatList
        data={transactionData}
        renderItem={({item}) => (
          <TransactionList data={item} navigation={navigation} />
        )}>
        <View style={{height: 20}}></View>
      </FlatList>
      <View style={{height: 20}}></View>
    </View>
  );
};

export default Transaction;
