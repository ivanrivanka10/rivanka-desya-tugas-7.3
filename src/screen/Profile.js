import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';

import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  style,
  TouchableOpacity,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';

const Profile = ({navigation}) => {
  const dispatch = useDispatch();
  const {loginData} = useSelector(state => state.login);
  return (
    <View style={{flex: 1, backgroundColor: '#F6F8FF', alignItems: 'center'}}>
      <View
        style={{
          width: '100%',
          height: 292,
          backgroundColor: '#FFFFFF',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          source={{uri: loginData.imgProfile}}
          style={{
            width: 95,
            height: 95,
            borderRadius: 15,
            marginTop: 52,
          }}></Image>
        <Text
          style={{
            fontSize: 20,
            fontWeight: '700',
            color: '#050152',
            marginTop: 5,
          }}>
          {loginData.nama}
        </Text>
        <Text
          style={{
            fontWeight: '500',
            fontSize: 10,
            color: '#A8A8A8',
            marginTop: 5,
          }}>
          {loginData.email}
        </Text>
        <View
          style={{
            height: 28,
            width: 65,
            backgroundColor: '#F6F8FF',
            borderRadius: 18,
            alignItems: 'center',
            marginTop: 24,
          }}>
          <TouchableOpacity
            style={{
              width: '100%',
              height: '100%',
              alignContent: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => navigation.navigate('EditProfile')}>
            <Text
              style={{
                color: '#050152',
                fontWeight: '600',
                fontSize: 12,
              }}>
              Edit
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          height: 293,
          width: 339,
          backgroundColor: 'white',
          marginTop: 12,
        }}>
        <TouchableOpacity
          style={{
            width: '100%',
            height: 59,
            backgroundColor: 'white',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontWeight: '500',
              color: '#000000',
              fontSize: 16,
              marginLeft: 79,
              textAlignVertical: 'center',
            }}>
            About
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            width: '100%',
            height: 59,
            backgroundColor: 'white',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontWeight: '500',
              color: '#000000',
              fontSize: 16,
              marginLeft: 79,
              textAlignVertical: 'center',
            }}>
            Term & Condition
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('FAQ')}
          style={{
            width: '100%',
            height: 59,
            backgroundColor: 'white',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontWeight: '500',
              color: '#000000',
              fontSize: 16,
              marginLeft: 79,
              textAlignVertical: 'center',
            }}>
            FAQ
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            width: '100%',
            height: 59,
            backgroundColor: 'white',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontWeight: '500',
              color: '#000000',
              fontSize: 16,
              marginLeft: 79,
              textAlignVertical: 'center',
            }}>
            History
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            width: '100%',
            height: 59,
            backgroundColor: 'white',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontWeight: '500',
              color: '#000000',
              fontSize: 16,
              marginLeft: 79,
              textAlignVertical: 'center',
            }}>
            Setting
          </Text>
        </TouchableOpacity>
      </View>

      <View
        style={{
          marginTop: 19,
          height: 47,
          width: 339,
          backgroundColor: 'white',
        }}>
        <TouchableOpacity
          onPress={() => {
            dispatch({type: 'LOGOUT'});
            navigation.replace('Login');
          }}
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'white',
            justifyContent: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon name="logout" size={15} color={'#EB5757'}></Icon>
            <Text
              style={{
                fontWeight: '500',
                color: '#000000',
                fontSize: 16,
                marginLeft: 4,
                textAlignVertical: 'center',
                color: '#EB5757',
              }}>
              Log out
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            dispatch({type: 'RESET_DATA_LOGIN'});
            navigation.replace('Login');
          }}
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: '#EB5757',
            justifyContent: 'center',
            marginTop: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon name="delete" size={15} color={'white'}></Icon>
            <Text
              style={{
                fontWeight: '500',
                color: '#000000',
                fontSize: 16,
                marginLeft: 4,
                textAlignVertical: 'center',
                color: 'white',
              }}>
              Delete Data
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Profile;
