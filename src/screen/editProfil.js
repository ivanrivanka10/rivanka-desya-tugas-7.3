import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {ScrollView, TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import ImagePicker from 'react-native-image-picker';

const EditProfile = ({navigation}) => {
  const {loginData} = useSelector(state => state.login);
  const [nama, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [adress, setAdress] = useState('');
  const dispatch = useDispatch();
  const [image, setImage] = useState(null);

  const handleImagePicker = () => {
    ImagePicker.showImagePicker(
      {
        title: 'Select Image',
        mediaType: 'photo',
        cancelButtonTitle: 'Cancel',
        takePhotoButtonTitle: 'Take Photo',
        chooseFromLibraryButtonTitle: 'Choose from Library',
      },
      response => {
        if (!response.didCancel && !response.error) {
          setImage(response.uri);
        }
      },
    );
  };
  const tambahData = () => {
    const data = {
      nama: nama ? nama : 'Rivanka Desya',
      email: email ? email : 'ivanrivanka10@gmail.com',
      phoneNumber: phoneNumber ? phoneNumber : '081393194809',
      address: adress ? adress : 'Jogja',
      imgProfile:
        'https://www.wfla.com/wp-content/uploads/sites/71/2023/02/16f3a80ae3e14d6aa902ca8e8763da90.jpg?w=2405&h=1440&crop=1',
    };
    dispatch({type: 'ADD_DATA_LOGIN', data: data});
    navigation.replace('bottom');
  };
  return (
    <View style={{flex: 1, alignItems: 'center', backgroundColor: '#F6F8FF'}}>
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 56,
          elevation: 5,
          flexDirection: 'row',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Icon
            name="arrowleft"
            size={20}
            color="#000000"
            style={{marginLeft: 25}}
          />
        </TouchableOpacity>
        <Text
          style={{
            fontWeight: '700',
            color: 'black',
            fontSize: 18,
            marginLeft: 14,
          }}>
          Edit Profile
        </Text>
      </View>
      <View
        style={{
          height: '100%',
          backgroundColor: 'white',
          width: '100%',
          marginTop: 2,
        }}>
        <Image
          source={{uri: loginData.imgProfile}}
          style={{
            width: 95,
            height: 95,
            borderRadius: 15,
            marginTop: 32,
            alignSelf: 'center',
          }}></Image>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            alignContent: 'center',
            justifyContent: 'center',
            marginTop: 10,
          }}>
          <TouchableOpacity onPress={handleImagePicker}>
            <Icon name="edit" color={'#BB2427'} size={24} />
            <Text
              style={{
                fontWeight: '400',
                marginLeft: 5,
                fontSize: 18,
                color: '#BB2427',
              }}>
              Edit foto
            </Text>
          </TouchableOpacity>
        </View>

        <Text
          style={{
            color: '#BB2427',
            fontWeight: '600',
            fontSize: 12,
            marginLeft: 12,
            marginTop: 55,
          }}>
          Nama
        </Text>
        <View
          style={{
            backgroundColor: '#F6F8FF',
            height: 45,
            width: '90%',
            borderRadius: 8,
            marginLeft: 12,
            marginTop: 11,
          }}>
          <TextInput
            placeholder="Name"
            defaultValue={loginData.nama}
            onChangeText={Text => setName(Text)}></TextInput>
        </View>
        <Text
          style={{
            color: '#BB2427',
            fontWeight: '600',
            fontSize: 12,
            marginLeft: 12,
            marginTop: 27,
          }}>
          Email
        </Text>
        <View
          style={{
            backgroundColor: '#F6F8FF',
            height: 45,
            width: '90%',
            borderRadius: 8,
            marginLeft: 12,
            marginTop: 11,
          }}>
          <TextInput
            placeholder="Email@gmail.com"
            defaultValue={loginData.email}
            onChangeText={Text => setEmail(Text)}></TextInput>
        </View>
        <Text
          style={{
            color: '#BB2427',
            fontWeight: '600',
            fontSize: 12,
            marginLeft: 12,
            marginTop: 27,
          }}>
          No.Hp
        </Text>
        <View
          style={{
            backgroundColor: '#F6F8FF',
            height: 45,
            width: '90%',
            borderRadius: 8,
            marginLeft: 12,
            marginTop: 11,
          }}>
          <TextInput
            placeholder="08123456789"
            defaultValue={loginData.phoneNumber}
            onChangeText={Text => setPhoneNumber(Text)}></TextInput>
        </View>
        <Text
          style={{
            color: '#BB2427',
            fontWeight: '600',
            fontSize: 12,
            marginLeft: 12,
            marginTop: 27,
          }}>
          Alamat
        </Text>
        <View
          style={{
            backgroundColor: '#F6F8FF',
            height: 45,
            width: '90%',
            borderRadius: 8,
            marginLeft: 12,
            marginTop: 11,
          }}>
          <TextInput
            placeholder="jl melati"
            defaultValue={loginData.address}
            onChangeText={Text => setAdress(Text)}></TextInput>
        </View>

        <View
          style={{
            height: 55,
            width: 335,
            backgroundColor: '#BB2427',
            borderRadius: 10,
            alignSelf: 'center',
            marginBottom: 48,
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 55,
            position: 'absolute',
            bottom: 48,
          }}>
          <TouchableOpacity
            onPress={tambahData}
            style={{
              height: '100%',
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: '700',
                color: 'white',
              }}>
              Simpan
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default EditProfile;
