import {BottomTabBarHeightCallbackContext} from '@react-navigation/bottom-tabs';
import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';

const TransactionList = ({navigation, data}) => {
  const RenderItem = ({item}) => (
    console.log('ini apaan', item),
    (
      <View>
        <Text
          style={{
            marginLeft: 10,
            marginTop: 13,
            fontWeight: '500',
            fontSize: 12,
            color: '#201F26',
          }}>
          {item.shoesType} - {item.color} - {item.size}
        </Text>

        <Text
          style={{
            marginLeft: 10,
            marginTop: 3,
            fontWeight: '400',
            fontSize: 12,
            color: '#201F26',
          }}>
          {item.service}
        </Text>
      </View>
    )
  );
  console.log('data flatlist 1', data);
  return (
    <View
      style={{
        width: 353,

        backgroundColor: '#FFFFFF',
        marginTop: 5,
        borderRadius: 8,
        elevation: 3,
        marginBottom: 10,
        marginHorizontal: 20,
      }}>
      <TouchableOpacity
        onPress={() => navigation.navigate('DetailTransaction', data)}>
        <Text
          style={{
            color: '#BDBDBD',
            fontSize: 12,
            fontWeight: '500',
            marginLeft: 10,
            marginTop: 18,
          }}>
          {data.date} {data.time}
        </Text>
        <FlatList
          data={data.cartData}
          renderItem={({item}) => <RenderItem item={item} />}
        />

        <View
          style={{
            flexDirection: 'row',
            marginLeft: 10,
            marginTop: 13,
            alignItems: 'center',
            marginBottom: 10,
          }}>
          <Text style={{fontWeight: '400', color: '#201F26', fontSize: 12}}>
            Kode Reservasi :
          </Text>
          <Text style={{fontWeight: '700', color: '#201F26', fontSize: 12}}>
            C{data.reservationCode}
          </Text>
          <View
            style={{
              backgroundColor: '#F29C1F29',
              width: 81,
              height: 21,
              borderRadius: 10.5,
              right: 7,
              position: 'absolute',
              alignItems: 'center',
              marginBottom: 20,
            }}>
            <Text
              style={{
                color: '#FFC107',
                fontWeight: '400',
                fontSize: 12,
                textAlignVertical: 'center',
                alignSelf: 'center',
                marginTop: 2,
              }}>
              Reserved
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};
export default TransactionList;
