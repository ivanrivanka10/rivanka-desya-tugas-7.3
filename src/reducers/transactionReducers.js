const initialState = {
  transactionData: [],
};

const transactionReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA_TRANSACTION':
      return {
        ...state,
        transactionData: action.data,
      };
    case 'RESET_DATA_TRANSACTION':
      return {
        ...state,
        transactionData: [],
      };
    default:
      return state;
  }
};

export default transactionReducers;
