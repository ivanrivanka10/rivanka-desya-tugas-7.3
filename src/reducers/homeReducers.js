const initialState = {
  storeData: [],
};

const homeReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA':
      return {
        ...state,
        storeData: action.data,
      };
    case 'RESET_DATA':
      return {
        ...state,
        storeData: [],
      };
    case 'DELETE_DATA':
      var newData = [...state.storeData];
      var findIndex = state.storeData.findIndex(value => {
        return value.idStore === action.idStore;
      });
      newData.splice(findIndex, 1);
      return {
        ...state,
        storeData: newData,
      };
    case 'UPDATE_DATA':
      var newData = [...state.storeData];
      var findIndex = state.storeData.findIndex(value => {
        return value.idStore === action.data.idStore;
      });
      newData[findIndex] = action.data;
      return {
        ...state,
        storeData: newData,
      };
    default:
      return state;
  }
};

export default homeReducers;
