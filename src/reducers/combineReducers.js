import {combineReducers} from 'redux';
import authReducer from './authReducers';
import dataReducer from './dataReducer';
import homeReducers from './homeReducers';
import cartReducers from './cartReducers';
import transactionReducers from './transactionReducers';
import loginReducers from './loginReducers';

const rootReducer = combineReducers({
  auth: authReducer,
  data: dataReducer,
  home: homeReducers,
  cart: cartReducers,
  transaction: transactionReducers,
  login: loginReducers,
});

export default rootReducer;
