const initialState = {
  cartData: [],
};

const cartReducers = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_DATA_CART':
      return {
        ...state,
        cartData: action.data,
      };
    case 'RESET_DATA_CART':
      return {
        ...state,
        cartData: [],
      };
    default:
      return state;
  }
};

export default cartReducers;
