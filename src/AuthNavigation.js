import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Login from './screen/Login';
import FAQ from './screen/FAQ';
import DetailProduct from './screen/DetailProduct';
import Cart from './screen/cart';
import EditProfile from './screen/editProfil';
import DetailTransaction from './screen/DetailTransaction';
import FormPemesanan from './screen/FormPemesanan';
import Register from './screen/Register';
import Voucher from './screen/Voucher';
import Summary from './screen/Summary';
import DetailVoucher from './screen/DetailVoucher';
import SuccesPayment from './screen/SuccesPayment';
import CheckOut from './screen/CheckOut';
import MyTabs from './screen/bottomNav';
import TambahToko from './screen/TambahToko';
import Routing from './Routing';

const Stack = createStackNavigator();
function AuthNavigation() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen
        name="bottom"
        component={MyTabs}
        screenOptions={{headerShown: false}}
        // options={{title: 'Overview'}}
      />
      <Stack.Screen name="Routing" component={Routing} />
      <Stack.Screen name="DetailProduct" component={DetailProduct} />
      <Stack.Screen name="Cart" component={Cart} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="DetailTransaction" component={DetailTransaction} />
      <Stack.Screen name="FAQ" component={FAQ} />
      <Stack.Screen name="FormPemesanan" component={FormPemesanan} />
      <Stack.Screen name="Voucher" component={Voucher} />
      <Stack.Screen name="Summary" component={Summary} />
      <Stack.Screen name="DetailVoucher" component={DetailVoucher} />
      <Stack.Screen name="SuccesPayment" component={SuccesPayment} />
      <Stack.Screen name="CheckOut" component={CheckOut} />
      <Stack.Screen name="TambahToko" component={TambahToko} />
    </Stack.Navigator>
  );
}
export default AuthNavigation;
