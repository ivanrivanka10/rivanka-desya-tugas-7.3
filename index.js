/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Routing from './src/Routing';
import App from './App';
import {LogBox} from 'react-native';

AppRegistry.registerComponent(appName, () => App);
LogBox.ignoreAllLogs();
